/**
 * E2ST :: The E2 Skinning Toolkit
 * Written and maintained by HomeMade Digital :: www.homemadedigital.com
 *
 *   // example handler for all pages
 *   $.e2.addHandler( /./, function(){
 * 	  $.e2.debug("Handler for all pages called");
 *   } );
 *
 *   // example handler for a specific page
 *   $.e2.addHandler( /^LoginRegister$/, function(){
 * 	  $.e2.debug("Handler for LoginRegister called");
 *   } );
 *
 *   // example handler for multiple pages
 *   $.e2.addHandler( /^Donate|LoginRegister$/, function(){
 * 	  $.e2.debug("Handler for Donate OR LoginRegister called");
 *   } );
 *
 */

//------------- Config ---------------

/*
 *
 * Document Ready
 *
 */
$(document).ready(function() {
    $.e2.init();
});

/*
 *
 * Config Options
 *
 */
$.e2.configure({
    debug: false,
    resetCSS: false,
    eventID: 1
});












/*
 *
 * Footer -  html
 *
 */

var footer = ' \
<div class="mobile-footer-wrapper clearfix"> \
	<div class="footer"> \
		<div class="footer-copy"> \
		    <ul id="footer-nav"> \
		        <li><a href="http://www.legacy.com.au/PrivacyPolicy" target="_blank">Privacy policy</a></li> \
		    </ul> \
		    <p class="copyright"> \
		        © Legacy Brisbane 2017. All rights reserved \
		    </p> \
        <p class="image"> \
          <img src="//legacy-au.s3.amazonaws.com/frontstream/img/logo.png" alt="" /> \
        </p> \
		</div> \
	</div> \
</div> \
';


/*
 *
 * Where the money goes -  html
 *
 */
var wtmg = '\
	<div id="wtmg-wrapper"> \
		<div id="wtmg-inner" class="clearfix"> \
			<h2>Where your money goes</h2> \
			<div id="wtmg-img"> \
				<img src="https://legacy-au.s3.amazonaws.com/frontstream/img/wtmg.jpg" /> \
			</div> \
			<div id="wtmg-copy"> \
				<p>Legacy is an organisation that provides services to Australian families suffering after the injury or death of a spouse or parent, during or after their defence force service. We currently care for around 80,000 widows and 1,800 children and disabled dependants throughout Australia.</p> \
				<a href="http://www.legacy.com.au/OurWork">Find out more ></a> \
			</div> \
		</div> \
		<div class="mask"></div> \
	</div> \
';

/*
 *
 * Donation price points - html
 *
 */
var pricepoints = '\
	<div id="pp-wrapper"> \
	<h3>I\'d like to give</h3> \
	<div id="price-points"> \
	    <div class="buttons"> \
	        <a href="#" class="button" data-value="60">$60</a> \
	        <a href="#" class="button" data-value="100">$100</a> \
	        <a href="#" class="button" data-value="200">$200</a> \
	    </div> \
	    <div class="own-amount"> \
	        <div class="left"> \
	            Or choose your own amount: \
	        </div> \
	        <div class="right"> \
	        	<div class="inner"> \
		            <div class="currency-symbol">$</div> \
		            <input type="text" id="own-amount"> \
	        	</div> \
	        </div> \
	    </div> \
	    <div class="clr"></div> \
	</div> \
	</div> \
';



//------------ Page Handlers ------------

/*
 *
 * Global
 *
 */
$.e2.addHandler(/./, function() {

    jQuery(document).ready(function($) {
        // add flag on test event
        if ($.e2.getUrlEventId() == '47819') {
            $('body').prepend('<div id="devevent">HMD TEST</div>');
            MAINSITE_LINK = 'https://legacy-anzac-day.hmd-dev.com/';
        }
        $('.homelink').attr('href', MAINSITE_LINK);
        $('body').append(footer);
        // point menu sign up link to the hub site
        $('#ctl00_hylRegisterTab').attr('href', MAINSITE_LINK + 'registration');
    });


});

/*
 *
 * Global
 *
 */
$.e2.addHandler(/^mobileEventInfo$/, function() {

    $(document).ready(function() {
        // point sign up link to the hub site
        $('#ctl00_mainContent_hylRegister').attr('href', MAINSITE_LINK + 'registration');
    });

});

/*
 *
 * Add custom sharing
 *
 */

var share = '\
	<div class="share-container"> \
		<h3>Share this page:</h3> \
		<a class="facebook" href="#">Share on Facebook</a> \
		<a class="twitter" href="#">Share on Twitter</a> \
		<a class="linkedin" href="#">Share on Linkedin</a> \
		<div class="clear">&nbsp;</div> \
	</div> \
';

/*
 *
 * Individual Fundraising page
 *
 */
$.e2.addHandler(/^mobilePersonalPage$/, function() {

    // artez seems to  use the same template for mobile home and fundraising
    // page, we need to check which page it is using elements on the page

    $('#fundraiser').append('<div id="mask"></div>');

    jQuery(document).ready(function($) {

        // add share links
        $('.aside-thermometer').after(share);

        // trigger addthis links
        $('.share-container .facebook').click(function(e) {
            e.preventDefault();
            $('.addthis_button_facebook').trigger('click');
        });
        $('.share-container .twitter').click(function(e) {
            e.preventDefault();
            $('.addthis_button_twitter').trigger('click');
        });
        $('.share-container .linkedin').click(function(e) {
            e.preventDefault();
            $('.addthis_button_linkedin').trigger('click');
        });

        $('.mobile-footer-wrapper').before(wtmg);

    });

});

/*
 *
 * Team Fundraising page
 *
 */
$.e2.addHandler(/^mobileTeamPage$/, function() {


    $('#fundraiser').append('<div id="mask"></div>');

    var teamId = $(document).getUrlParam("teamID") || $(document).getUrlParam("TeamID") || $(document).getUrlParam("TSID");
    var teamname = $('#ctl00_mainContent_fundraiserSection').prev('h2').text().trim();
    $(document).ready(function() {});

    // add a signup link for joining team
    $('#ctl00_mainContent_hylDonate').after('<a href="' + MAINSITE_LINK + 'registration#/?teamID=' + teamId + '&teamname=' + teamname + '" class="primary signup-team">JOIN THIS TEAM</a>');

    jQuery(document).ready(function($) {

        // add share links
        $('.aside-thermometer').after(share);

        // trigger addthis links
        $('.share-container .facebook').click(function(e) {
            e.preventDefault();
            $('.addthis_button_facebook').trigger('click');
        });
        $('.share-container .twitter').click(function(e) {
            e.preventDefault();
            $('.addthis_button_twitter').trigger('click');
        });
        $('.share-container .linkedin').click(function(e) {
            e.preventDefault();
            $('.addthis_button_linkedin').trigger('click');
        });

        $('.mobile-footer-wrapper').before(wtmg);

    });

});

/*
 *
 * Sponsor page
 *
 */
$.e2.addHandler(/^mobileDonate|MobileDonate/, function() {

    // reorder some fields
    $('#ctl00_mainContent_txtEmail').parent().parent('.field').addClass('email').insertAfter('#ctl00_mainContent_contactEdit > div:eq(2)');

    // add some form labels
    $('#ctl00_mainContent_titleSection label span').appendTo($('#ctl00_mainContent_titleSection label'));
    $('#ctl00_mainContent_txtFirstName').closest('.field').prepend('<label>First name<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_txtLastName').closest('.field').prepend('<label>Last name<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_txtEmail').closest('.field').prepend('<label>Email<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_txtAddrLine1').closest('.field').prepend('<label>Address line 1<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_txtAddrLine2').parent().before('<label>Address line 2</label>');
    $('#ctl00_mainContent_txtCity').closest('.field').prepend('<label>Town/city<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_ddlCountry').closest('.field').prepend('<label>Country<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_ddlProvince').closest('.field').prepend('<label>State/territory<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_txtProvince').closest('.field').prepend('<label>Province/state</label>');
    $('#ctl00_mainContent_txtPostalCode').closest('.field').prepend('<label>Postcode<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_txtTelephoneNumber').closest('.field').prepend('<label>Phone number</label>');
    $('#ctl00_mainContent_ucMobilePayments_txtCreditCardNumber').closest('.field').prepend('<label>Card number<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_ucMobilePayments_txtCreditCardVerification').closest('.field').prepend('<label>Card security number<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_ucMobilePayments_txtCardHolderName').closest('.field').prepend('<label>Card holder name<span class="requiredMarker">*</span></label>');
    $('#ctl00_mainContent_ucMobilePayments_divExpiryDate').prepend('<label>Expiry date<span class="requiredMarker">*</span></label>');

    $('#ctl00_mainContent_txtAddrLine1').parent().addClass('mb20');

    // add form divider
    $('.field.email').after('<div class="divider"></div>');

    // add card type functionality
    var cardtypes = '\
		<p class="bucket">Please select how you\'d like to pay.</p> \
		<div id="cardtype"> \
			<div class="card-type visa">Visa</div> \
			<div class="card-type mastercard">MasterCard</div> \
			<div class="card-type amex">Amex</div> \
		</div> \
	';

    $('#ctl00_mainContent_divlitPaymentInformation').before(cardtypes);


    $(document).ready(function($) {

        $('#ctl00_mainContent_facebookPanel').before(pricepoints);

        $('#price-points .button').bind().click(function(e) {
            e.preventDefault();
            $('#price-points .button').removeClass('active');
            $(this).addClass('active');
            var amount = $(this).data('value');
            $('#ctl00_mainContent_ucMobileDonationAmount_tbCustomAmount, #ctl00_mainContent_ucMobileDonationAmount_hdCustomAmount, #own-amount').val(amount);
        });

        $('#own-amount').change(function() {

            var amount = $(this).val();

            // accept whole numbers only
            if (Number(amount) !== parseInt(amount, 10) || Number(amount) < 0) {
                alert('Please enter whole numbers only');
                $('#own-amount').focus();
                return false;
            } else {
                $('#ctl00_mainContent_ucMobileDonationAmount_tbCustomAmount, #ctl00_mainContent_ucMobileDonationAmount_hdCustomAmount, #own-amount').val(amount);
            }

            // add active class to price point if manual entry is == to one of them
            $('#price-points .button').removeClass('active');
            $('#price-points .button').each(function() {
                var price_points = $(this).data('value');
                if (Number(amount) === price_points) {
                    $(this).addClass('active');
                }
            });

        });

        // remove inline styling on cc fields container
        $('#ctl00_mainContent_divPaymentInformation').removeAttr('style');

        $('.card-type').click(function() {

            $('.card-type').removeClass('active');
            $(this).addClass('active');

            if ($(this).text() === 'Paypal') {

                // set paypal route
                $('#usePayPalExpressCheckout').trigger('click');

            } else {

                // set cc route
                // $('#useTraditionalGateway').trigger('click');

                // set hidden card type select
                $("#ctl00_mainContent_ucMobilePayments_ddlCreditCardType0").val($(this).text()).change();
                $('#ctl00_mainContent_divPaymentInformation').show();

            }

        });


        // set default price point if there are no validation errors
        if ($('#mobiledonate div.error-notice').length === 0) {

            $('#price-points .buttons .button:eq(1)').trigger('click');

        } else {

            // there are validation errors, so we need to set the states of custom elements
            var amount = Number($('#ctl00_mainContent_ucMobileDonationAmount_tbCustomAmount').val());
            if (amount > 0) {
                $('#own-amount').val(amount);
                // set price point if amount matches
                $('#price-points .button').each(function() {
                    if ($(this).data('value') === amount) {
                        $(this).trigger('click');
                    }
                });
            }

            // set payment type to paypal
            if ($('#ctl00_mainContent_hdUsePayPal').val() === 'y') {

                $('.card-type.paypal').trigger('click');

            // set payment type to credit card
            } else {

                // set cc route
                // $('#useTraditionalGateway').trigger('click');

                // set cc type
                var cardtype = $("#ctl00_mainContent_ucMobilePayments_ddlCreditCardType0").val();
                $('.card-type').each(function() {
                    if ($(this).text() === cardtype) {
                        $(this).trigger('click');
                    }
                });

            }

        }


    });

});


/*
 *
 * Search page
 *
 */
$.e2.addHandler(/^MobileIndividualAndTeamSearchPage$/, function() {

    $('#ctl00_mainContent_searchForTeamOrIndividual').parent().addClass('search');

    // add a custom toggle
    $('.bucket.search').before('<ul id="search-toggle"><li data-id="Individual" class="active">Sponsor a person</li><li data-id="Team">Sponsor a team</li></ul>');

    $('#search-toggle li').click(function() {
        var id = $(this).data('id');
        // set hidden select
        $("#ddlSelectSearchType").val(id).change();
        $('#search-toggle li').removeClass('active');
        $(this).addClass('active');
    });

});


/****************************************************************************************************************************/
/*  Page functions                                                                                                          */
/****************************************************************************************************************************/
function addCommas(amount) {

    amount += '';
    var splitAmount = amount.split('.');
    var dollars = splitAmount[0];
    var cents = splitAmount.length > 1 ? '.' + splitAmount[1].substr(0, 2) : '';
    var regex = /(\d+)(\d{3})/;
    while (regex.test(dollars)) {
        dollars = dollars.replace(regex, '$1' + ',' + '$2');
    }
    return '$' + dollars;

}
