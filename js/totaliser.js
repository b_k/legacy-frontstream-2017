$(document).ready(function() {

  if ($('body').hasClass('e2-TeamFundraisingPage') || $('body').hasClass('e2-FundraisingPage') || $('body').hasClass('e2-CorporateTeamFundraisingPage')) {

    tTotal = parseInt($('.pieBlock1 b:eq(0)').text().replace('$', '').replace(/\,/g, ''));
    tRaised = parseInt($('.pieBlock1 b:eq(1)').text().replace('$', '').replace(/\,/g, ''));
    tPercent = parseInt($('.pieBlock2 b:eq(0)').text().replace('%', ''));

    injectTotaliser(tTotal, tRaised, tPercent);

  }

  if ($('body').hasClass('e2-mobilePersonalPage') || $('body').hasClass('e2-mobileTeamPage')) {

    var percent = parseInt($('#statistics #percentValue').text().replace('%', '').replace('.00', ''));
    var goal = parseInt($('#statistics #goalValue strong').text().replace('$', '').replace('.00', '').replace(/\,/g, ''));
    var raised = parseInt($('#statistics p:last strong').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));

    $('#ctl00_mainContent_goalEnabled').prepend('<div class="aside-thermometer"></div>');

    injectTotaliser(goal, raised, percent);

  }

  if ($('body').hasClass('e2-CheckResults') || $('body').hasClass('e2-ManageSponsors')) {

    var percent = $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceParticipant_labelTotalAmountPercentage').text().replace('%', '');
    var goal = $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceParticipant_labelFundraisingGoalValue').text().replace('$', '').replace(/\,/g, '').replace('.00', '');
    var raised = Math.round($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceParticipant_labelTotalAmountValue').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));

    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceParticipant_divPerformanceProgressChart').html('<div class="aside-thermometer"></div>');

    injectTotaliser(goal, raised, percent);

  }

  if ($('body').hasClass('e2-TeamResults')) {

    var percent = parseInt($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformance_labelTeamTotalAmountPercentage').text().replace('%', ''));
    var goal = parseInt($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformance_labelTeamFundraisingGoalValue').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));
    var raised = Math.round($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformance_labelTeamTotalAmountRaisedValue').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));

    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformance_divPerformanceProgressChart').html('<div class="aside-thermometer"></div>');

    injectTotaliser(goal, raised, percent);

  }

  if ($('body').hasClass('e2-ManageTeamSponsors')) {

    var percent = parseInt($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceTeam_labelTeamTotalAmountPercentage').text().replace('%', ''));
    var goal = parseInt($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceTeam_labelTeamFundraisingGoalValue').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));
    var raised = Math.round($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceTeam_labelTeamTotalAmountRaisedValue').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));

    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceTeam_divPerformanceProgressChart').html('<div class="aside-thermometer"></div>');

    injectTotaliser(goal, raised, percent);

  }

  if ($('body').hasClass('e2-CorporateTeamResults')) {

    var percent = parseInt($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformance_labelCorporateTeamTotalAmountPercentage').text().replace('%', ''));
    var goal = parseInt($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformance_labelCorporateTeamFundraisingGoalValue').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));
    var raised = Math.round($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformanceTeam_labelTeamTotalAmountRaisedValue').text().replace('$', '').replace(/\,/g, '').replace('.00', ''));

    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_ucPerformance_ucProgressChart').html('<div class="aside-thermometer"></div>');

    injectTotaliser(goal, raised, percent);

  }

});

// set svg stroke percent
function setPercentageTo(percent) {
  var path = $('#percent-path').get(0);
  var pathLen = path.getTotalLength();
  var adjustedLen = percent * pathLen / 100;
  path.setAttribute('stroke-dasharray', adjustedLen + ' ' + pathLen);
}

// inject totaliser
function injectTotaliser(goal, raised, percent) {

  // fix for Artez rounding up % values
  var pc = raised / goal * 100;
  percent = Math.floor(pc);

  // atrez totaliser can have values higher than 100%
  if (percent > 100) {
    percent = 100;
  }


  var thermo = ' \
		<div class="thermo"> \
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 460 495" enable-background="new 0 0 460 495" xml:space="preserve"> \
		  	<path id="bg" stroke="#f6f6f6" stroke-width="30" fill="none" d="M230,29.6c100.1,0,181.2,58.1,181.2,129.6c0,24.8-9.8,48-26.8,67.8c29.8,25.3,47.8,57.8,47.9,93.4 c0,79.9-90.6,144.7-202.2,144.7c-111.7,0-202.2-64.8-202.2-144.7c0-35.6,17.9-68.2,47.8-93.4c-17-19.8-26.8-43-26.8-67.8 C48.9,87.7,129.9,29.6,230,29.6"/> \
		  	<path id="percent-path" stroke="#f12b15" stroke-width="30" fill="none"  d="M230,29.6c100.1,0,181.2,58.1,181.2,129.6c0,24.8-9.8,48-26.8,67.8c29.8,25.3,47.8,57.8,47.9,93.4 c0,79.9-90.6,144.7-202.2,144.7c-111.7,0-202.2-64.8-202.2-144.7c0-35.6,17.9-68.2,47.8-93.4c-17-19.8-26.8-43-26.8-67.8 C48.9,87.7,129.9,29.6,230,29.6"/> \
			</svg> \
			<div class="percent"><span></span>%</div> \
			<div class="raised"><span></span> raised of</div> \
			<div class="goal"><span></span> target</div> \
	  </div> \
	';

  $('.aside-thermometer').prepend(thermo);

  $(document).ready(function() {
    $('.thermo .goal span').html(addCommas(goal));
    $('.thermo .raised span').html(addCommas(raised));
    $('.thermo .percent span').html(percent);
    // animate the totaliser
    setPercentageTo(0);
    $('#percent-path').hide();
    setTimeout(function() {
      $('#percent-path').hide();
      $('#percent-path').show();
      setPercentageTo(percent);
    }, 1000);

  });

}
