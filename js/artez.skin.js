/**
 * E2ST :: The E2 Skinning Toolkit
 * Written and maintained by HomeMade Digital :: www.homemadedigital.com
 *
 *   // example handler for all pages
 *   $.e2.addHandler( /./, function(){
 * 	  $.e2.debug("Handler for all pages called");
 *   } );
 *
 *   // example handler for a specific page
 *   $.e2.addHandler( /^LoginRegister$/, function(){
 * 	  $.e2.debug("Handler for LoginRegister called");
 *   } );
 *
 *   // example handler for multiple pages
 *   $.e2.addHandler( /^Donate|LoginRegister$/, function(){
 * 	  $.e2.debug("Handler for Donate OR LoginRegister called");
 *   } );
 *
 */


/*
 *
 * Document Ready
 *
 */
$(document).ready(function() {
    $.e2.init();
});

/*
 *
 * Config Options
 *
 */
$.e2.configure({
    debug: false,
    resetCSS: false,
    eventID: 1
});


//------------ Page Handlers ------------

/*
 *
 * Global
 *
 */
$.e2.addHandler(/./, function() {

    // add flag on test event
    if ($.e2.getUrlEventId() == '47819') {
        $('body').prepend('<div id="devevent">HMD TEST</div>');
        MAINSITE_LINK = 'https://legacy-anzac-day.hmd-dev.com/';
        HOME_LINK = 'http://test-legacy-anzac-day-2016.pantheonsite.io/';
    } else {
        HOME_LINK = MAINSITE_LINK;
    }

    $('#ctl00_ctl00_headerContent_linkButtonDonate').attr('href', 'https://lsportal.sydney-legacy.com/legacydonationportal/account/default.aspx');
    // if ($('#fundraising-navigation').length) {
    //   $('#fundraising-navigation').prepend('<div id="pre-nav"></div>').append('<div id="post-nav"></div>');
    // }

    $('title').after('<link rel="icon" href="//legacy-au.s3.amazonaws.com/frontstream/img/favi.png" />');

    // header - create a consistent container for header links
    if ($('header#header').length > 0) {

        $('#header').wrap('<div id="header-outer-wrapper"><div id="header-wrapper"></div>').prepend('<a class="homelink" href="https://secure.legacybrisbane.com.au/registrant/startup.aspx?eventid=' + jQuery.e2.getUrlEventId() + '"><img src="//legacy-au.s3.amazonaws.com/frontstream/img/logo.png" alt="" /></a><div id="header-links"></div></div>');

        $('#header > a:not(.homelink, .nbcflink)').each(function() {
            $(this).appendTo('#header-links');
        });

        $('#header .profile').appendTo('#header-links');

        // add a search link
        var searchlink = 'https://secure.artezpacific.com/registrant/search.aspx?eventID=' + jQuery.e2.getUrlEventId() + '&Lang=en-CA';
        $('#header-links .profile').append('<a href="' + searchlink + '">Search</a>');

        // move fb image
        $('img.photo-mask').prependTo('#ctl00_ctl00_headerContent_hyperlinkMenu_logout');

    }

    // add body class to signal successful initialisation
    $("body").addClass('e2st-active');


});



/*
 *
 * Event home / login page
 *
 */
$.e2.addHandler(/^LoginRegister$/, function() {

    // make event homepage the login page
    $('#loginregister').appendTo('#scoreboard');
    $('#login').append('<p class="or"><span>Or</span></p><p><a class="facebook-faux-button" href="#">Log in with Facebook</a></p>');
    $('.facebook-faux-button').click(function() {
        loginWithFacebook();
        return false;
    });
    $('.help').insertBefore('#ctl00_ctl00_mainContent_cphLoginRegister_ucLogin_btnLogin');

    // point sign up link to the hub site
    $('.sign-up .button').attr('href', MAINSITE_LINK + 'registration#/');


});


/*
 *
 * Fundraising Home
 *
 */
$.e2.addHandler(/^Home|home$/, function() {

    $('#shortcut-links-container').prepend('<h3>Take action</h3>')

    // get image ID
    var imgageSrc;
    if ($('.media-manager-list li').length > 0) { // image has been uploaded
        if ($('.media-manager-list li').length === 1) { // only one image
            imgageSrc = '/registrant/' + $('.media-manager-list li img').attr('src');
        } else { // more than one image, grab the latest uploaded
            imgageSrc = '/registrant/' + $('.media-manager-list li:eq(0) img').attr('src');
        }
    }

    $('h3.fr-page').prependTo('#section-personalization');

    // replace the media carousel with a single image
    $(document).ready(function($) {
        $('#media-manager-container').html('<div id="profile-pic-wrapper"><div id="profile-pic" style="background:url(' + imgageSrc + ')"></div></div>');
    });

});


/*************************************************************************
*  Fundraising pages
*************************************************************************/

/*
 *
 * Funsraising Home
 *
 */
$.e2.addHandler(/^CheckResults$/, function() {

    // rewrite instructions for image uploads
    $('.media-item-edit-label-advice').html('<p><strong>Note:</strong> For best results, we recommend uploading an image that is 600 pixels wide and 600 pixels tall.</p>');

});
/*
 *
 * Get sponsors
 *
 */
$.e2.addHandler(/^solicitOthers$/, function() {});
/*
 *
 * View sponsor list
 *
 */
$.e2.addHandler(/^solicitList$/, function() {});




/*
 *
 * View sponsors
 *
 */
$.e2.addHandler(/^ViewSponsors$/, function() {});

/*
 *
 * Thank sponsors
 *
 */
$.e2.addHandler(/^thankSponsors$/, function() {});

/*
 *
 * Pay in money
 *
 */
$.e2.addHandler(/^offlineDonation|OfflineDonation$/, function() {});


/*
 *
 * Pay offline donations
 *
 */
$.e2.addHandler(/^PayOfflineDonations$/, function() {});
/*
 *
 * Pay offline donations confirm
 *
 */
$.e2.addHandler(/^PayOfflineDonationsConfirm$/, function() {});

// self sponsor price points
var sspp = '\
	<table id="price-points"> \
		<tr> \
			<td class="pp pp1" data-value="60"> \
				<div class="label">$60</div> \
				<label>$60 can pay for a family to be connected to the internet and access the web</label> \
			</td> \
			<td class="spacer"></td> \
			<td class="pp pp2" data-value="100"> \
				<div class="label">$100</div> \
				<label>$100 can provide a family with heating to keep warm in winter</label> \
			</td> \
			<td class="spacer"></td> \
			<td class="pp pp3" data-value="200"> \
				<div class="label">$200</div> \
				<label>$200 can provide trusted expert advice on family estate matters</label> \
			</td> \
		</tr> \
		<tr> \
			<td colspan="5" id="choose-amount"> \
				<div class="own-label">or choose your own amount*</div> \
				<div class="own-amount"> \
					<div class="currency-symbol">$</div> \
					<div class="amount"><input type="text" id="own-amount" /></div> \
					<div class="decimals">.00</div> \
				</div> \
			</td> \
		</tr> \
	</table> \
';

/*
 *
 * Self sponsor page
 *
 */
$.e2.addHandler(/^selfSponsor$/, function() {

    // insert price points
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_paymentCaption').before(sspp);

    // price point click functionality
    $('.pp').click(function() {
        var amount = $(this).data('value');
        $('#own-amount, #ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ucDonationAmount_tbCustomDonationAmount').val(amount);
        $('.pp').removeClass('active');
        $(this).addClass('active');
    });;

    // manual amount entry functions
    $('#own-amount').change(function() {

        var amount = $(this).val();

        // accept whole numbers only
        if (Number(amount) !== parseInt(amount, 10) || Number(amount) < 0) {
            alert('Please enter whole numbers only');
            $('#own-amount').focus();
            return false;
        }

        // add active class to price point if manual entry is == to one of them
        $('.pp').removeClass('active');
        $('.pp').each(function() {
            var price_points = $(this).data('value');
            if (Number(amount) === price_points) {
                $(this).addClass('active');
            }
        });

        $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ucDonationAmount_tbCustomDonationAmount').val(amount);

    });

    // only set these if there are no validation errors, else it will change user input values
    if ($('#error').text().length === 0) {

        // set middle price-point
        $('.pp2').trigger('click');

    } else {

        $(document).ready(function() {

            var amount = $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ucDonationAmount_tbCustomDonationAmount').val();
            $('#own-amount').val(amount);

            $('.pp').removeClass('active');
            $('.pp').each(function() {
                var price_points = $(this).data('value');
                if (Number(amount) === price_points) {
                    $(this).addClass('active');
                }
            });

            // show cc fields if cc selected else hide them if PayPal selected
            if ($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_useTraditionalGateway').prop('checked')) {
                // set card type
                var cardtype = $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ddlCreditCardType0').val();
                $('.card-type').each(function() {
                    if ($(this).text() === cardtype) {
                        $(this).trigger('click');
                    }
                });
            } else {
                $('.card-type.paypal').trigger('click');
            }

        });

    }

    // move optins
    $('#article-content table').last().addClass('optins').insertAfter('#price-points');

    // add card options
    $('#tbodyPayPalExpressCheckout').before(cardtypes);

    // disable pay button
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_btnProceed').attr('disabled', 'disabled');

    $('.card-type').click(function() {

        $('.card-type').removeClass('active');
        $(this).addClass('active');

        if ($(this).text() === 'Paypal') {

            // set paypal route
            $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_usePayPalExpressCheckout').trigger('click');

            $('#tbodyCC').hide();

        } else {

            // set cc route
            $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_useTraditionalGateway').trigger('click');

            // set hidden card type select
            $("#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ddlCreditCardType0").val($(this).text()).change();

            $('#tbodyCC').show();

        }

        // enable submit button
        $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_btnProceed').removeAttr('disabled');
    });

    $(document).ready(function() {
        $('#trAmount, #tbodyPayPalExpressCheckout, #tbodyCC').removeAttr('style');
    });
});

/*
 *
 * self sponsor confirm
 *
 */
$.e2.addHandler(/^selfSponsorConfirm$/, function() {});


/*
 *
 * Edit goal page
 *
 */
$.e2.addHandler(/^PersonalSetup$/, function() {});

/*
 *
 * Edit personal page
 *
 */
$.e2.addHandler(/managePersonalPage/, function() {});

/*
 *
 * Customise personal page
 *
 */
$.e2.addHandler(/^customizePersonalPage$/, function() {});




/*************************************************************************
*  My Team pages
*************************************************************************/

/*
 *
 * team create
 *
 */
$.e2.addHandler(/teamCreate$/, function() {});

/*
 *
 * join a team
 *
 */
$.e2.addHandler(/^teamOptions$/, function() {});

/*
 *
 * join a team confirmation
 *
 */
$.e2.addHandler(/^joinTeamConfirmation$/, function() {});

/*
 *
 * team progress
 *
 */
$.e2.addHandler(/^TeamResults$/, function() {

    // rewrite instructions for image uploads
    $('.media-item-edit-label-advice').html('<p><strong>Note:</strong> For best results, we recommend uploading an image that is 600 pixels wide and 600 pixels tall.</p>');

});

/*
 *
 * recruit members or sopnsors
 *
 */
$.e2.addHandler(/^solicitTeamMembers$/, function() {});

/*
 *
 * email team members
 *
 */
$.e2.addHandler(/^teamSendEmail$/, function() {});

/*
 *
 * fundraise with your workplace
 *
 */
$.e2.addHandler(/^TeamQuestionsEdit$/, function() {});

/*
 *
 * edit team info
 *
 */
$.e2.addHandler(/^teamEdit$/, function() {});

/*
 *
 * edit team page
 *
 */
$.e2.addHandler(/^manageTeamPage$/, function() {});

/*
 *
 * view team sponsors page
 *
 */
$.e2.addHandler(/^ViewTeamSponsors$/, function() {});

/*
 *
 * pay in money page
 *
 */
$.e2.addHandler(/teamOfflineDonation/, function() {});

/*
 *
 * view recruitment list page
 *
 */
$.e2.addHandler(/solicitTeamList/, function() {});

/*
 *
 * Group message board
 *
 */
$.e2.addHandler(/^teamMessageBoard$/, function() {});



/*
 *
 * Group leader report
 *
 */
$.e2.addHandler(/^teamCaptainReport$/, function() {});






/*************************************************************************
*  My Profile pages
*************************************************************************/

/*
 *
 * edit contact information page
 *
 */
$.e2.addHandler(/^UserContactInfo$/, function() {});

/*
 *
 * communication preferences
 *
 */
$.e2.addHandler(/^UserQuestionsEdit$/, function() {});

/*
 *
 * new password page
 *
 */
$.e2.addHandler(/^NewPassword$/, function() {});

/*
 *
 * Session expired page
 *
 */
$.e2.addHandler(/^sessionExpired$/, function() {

    // write links with dynamic event id
    var searchLink = $('#ctl00_ctl00_headerContent_linkButtonSearch').attr('href');
    var loginLink = '/registrant/LoginRegister.aspx?eventid=' + $(document).getUrlParam("eID") + '&LanguageCode=en-CA';
    $('#buttons .login').attr('href', loginLink);
    $('#buttons .search').attr('href', searchLink);
    $('#buttons .mainsite').attr('href', MAINSITE_LINK);

});



// add card type functionality
var cardtypes = '\
	<tbody id="cardtype"> \
		<tr id="cardtype"> \
			<th><label for="">Card type </label><span class="req">*</span></th> \
			<td> \
				<div class="card-type visa">Visa</div> \
				<div class="card-type mastercard">MasterCard</div> \
				<div class="card-type amex">Amex</div> \
				<div class="card-type paypal">Paypal</div> \
			</td> \
		</tr> \
	</tbody> \
';

/*
 *
 * Donate page
 *
 */
$.e2.addHandler(/^donate|Donate$/, function() {

    // hubsite links
    $('.homelink').attr('href', HOME_LINK);

    // Move price-points to top of page
    $('.note').after('<table id="suggested-amounts"></table>');
    $('article .pagegroup:last table:eq(0) tbody:eq(0)').appendTo('#suggested-amounts');
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_trDonorListingNameConsent').appendTo('#suggested-amounts');
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_trDonorListingAmountConsent').appendTo('#suggested-amounts');

    // set title for resonal/org donation
    $('#suggested-amounts').after('<h2>I\'m donating...</h2>');

    // add currency symbol
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ucDonationAmount_tbCustomDonationAmount').before('<span class="currency-symbol">$</span>').after('<span class="decimals">.00</span>');

    // clear floats
    $('#divrdbOtherAmount, .suggestedContentAmountRow').addClass('clearfix');

    // add styling to price-points
    $('.suggestedContentAmountRow label').each(function() {
        var amt = $(this).prev('input[type=radio]').val();
        $(this).wrap('<div class="label-wrapper"></div>');
        $(this).before('<div class="label">$' + amt + '</div>');
    });

    $(document).ready(function() {
        $('#divOrganizationDonation, #divPersonalDonation').removeAttr('style');
    });

    // add functionality to price-points
    $('.suggestedContentAmountRow .label-wrapper').bind().click(function(e) {
        e.preventDefault();
        var amt = $(this).prev('input[type=radio]').val();
        $('.suggestedContentAmountRow .label-wrapper').removeClass('active');
        $(this).addClass('active');
        $('input#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ucDonationAmount_tbCustomDonationAmount').val(amt);
    });

    // manual amount entry functions
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ucDonationAmount_tbCustomDonationAmount').change(function() {

        var amount = $(this).val();

        // accept whole numbers only
        if (Number(amount) !== parseInt(amount, 10) || Number(amount) < 0) {
            alert('Please enter whole numbers only');
            $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ucDonationAmount_tbCustomDonationAmount').focus();
            return false;
        }

        // add active class to price point if manual entry is == to one of them
        $('.suggestedContentAmountRow .label-wrapper').removeClass('active');
        $('.suggestedContentAmountRow .label-wrapper').each(function() {
            var price_points = $(this).prev().val();
            if (amount === price_points) {
                $(this).addClass('active');
            }
        });

    });

    // set personal or org donation
    $('#divOrganizationDonation').appendTo($('fieldset'));
    $('#divPersonalDonation').appendTo($('fieldset'));

    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_lblMakePersonalDonation').click(function() {
        $(this).addClass('active');
        $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_lblMakeOrganizationDonation').removeClass('active');
    });
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_lblMakeOrganizationDonation').click(function() {
        $(this).addClass('active');
        $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_lblMakePersonalDonation').removeClass('active');
    });

    // contact details - rearrange fields
    $('article > table:eq(2)').addClass('contact-details');

    // move email address to contact details
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_contactInfoControl_trEmailAddress').prependTo('.contact-details tbody');

    // move mail opt in
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_trChkSendMailConsent').addClass('mail-optin').insertAfter('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_contactInfoControl_trPostalCode');

    // move email opt in
    $('.contact-details tbody tr').last().addClass('email-optin').insertAfter('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_contactInfoControl_trEmailAddress');

    // move phone number field
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_contactInfoControl_trPhoneNumber').addClass('phone-number').insertAfter('.email-optin');

    // add heading for billing address
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_contactInfoControl_trAddress1').before('<tr class="billing-address"><td colspan="2"><h2>Billing address</h2></td></tr>');

    // add a spacer class
    $('.phone-number').next('tr').addClass('h70');

    // add description to phone number field
    $('.phone-number').after('<tr><td colspan="2"><p>It\'s helpful for us to have your phone number in case we need to contact you if there\'s an issue with your payment.</p></td></tr>');

    // disable submit button
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_btnProceed').attr('disabled', 'disabled');

    // move mandatory asterisks
    $('th span.req').each(function() {
        $(this).insertAfter($(this).next('label'));
    });



    $(document).ready(function() {

        $('#tbodyCC').removeAttr('style').before(cardtypes);

        $('.card-type').click(function() {

            $('.card-type').removeClass('active');
            $(this).addClass('active');

            if ($(this).text() === 'Paypal') {

                // set paypal route
                $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_usePayPalExpressCheckout').trigger('click');

                // hide cc fields
                $('#tbodyCC').hide();

            } else {

                // set cc route
                // $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_useTraditionalGateway').trigger('click');

                // set hidden card type select
                $("#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ddlCreditCardType0").val($(this).text()).change();

                // show cc fields
                $('#tbodyCC').show();

            }

            // enable submit button
            $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_btnProceed').removeAttr('disabled');
        });

    });


    // only set these if there are no validation errors, else it will change user input values
    if ($('#error').text().length === 0) {

        // set middle price-point
        $('.suggestedContentAmountRow:eq(1) .label-wrapper').trigger('click');

        // need to scroll top as page doesn't always load at the top of the screen
        $(window).scrollTop(0);

    } else {

        $(document).ready(function() {
            // because of the customisation we need to restore form states here

            // clean up errors html
            $('#error').html($('#error').html().replace(/&nbsp;/gi, ''));

            // need to set personal or org donation if there are errors
            if ($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_txtOrganization').val() === '') {
                $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_lblMakePersonalDonation').trigger('click');
            } else {
                $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_lblMakeOrganizationDonation').trigger('click');
            }

            // add a close button for error panel because it's absolutely positioned so as not to break the page design
            $('#error').prepend('<h2>Please check the following errors</h2>').prepend('<div id="close">X</div>');
            $('#close').bind().click(function() {
                $('#error').hide();
            });

            // show cc fields if cc selected else hide them if PayPal selected
            if ($('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_useTraditionalGateway').prop('checked')) {
                // set card type
                var cardtype = $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_paymentControl_ddlCreditCardType0').val();
                $('.card-type').each(function() {
                    if ($(this).text() === cardtype) {
                        $(this).trigger('click');
                    }
                });
            } else {
                $('.card-type.paypal').trigger('click');
            }

            // enable submuit button
            $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_btnProceed').removeAttr('disabled');

            // need to scroll top as page doesn't always load at the top of the screen
            $(window).scrollTop(0);
        });
    }


});

/*
 *
 * Donate artez wait page
 *
 */
$.e2.addHandler(/^wait$/, function() {});


/*
 *
 * Donate confirm page
 *
 */
$.e2.addHandler(/^donateConfirm$/, function() {

    // $('#article-content').after('<div id="donate-side-pic"><img src="https://legacy-au.s3.amazonaws.com/frontstream/img/donate.jpg" /></div>');


});

/*
 *
 * Donate thank you page
 *
 */
$.e2.addHandler(/^DonateThankYou$/, function() {

    // move submit button
    // $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_cmdSubmitMessage').appendTo('#inputContainer table tr:last td:last');

    $(document).ready(function() {
        // set maxlength for messages
        $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_txtMessageInput')
            .val('')
            .attr('placeholder', 'Leave a message...')
            .attr('maxlength', '200')
            .after('<div class="counter-container"><div>');

        $("#ctl00_ctl00_mainContent_bodyContentPlaceHolder_txtMessageInput").maxlength({
            counterContainer: $(".counter-container"),
            text: '%left characters left'
        });

        $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_txtNameInput')
            .val('')
            .attr('placeholder', 'Enter your name');
    });

    $('#pagelink a').append('’s Page');

    $('#article-content').after('<div id="donate-side-pic"><img src="https://legacy-au.s3.amazonaws.com/frontstream/img/default.png" /></div>');

});



/*
 *
 * Where the money goes -  html
 *
 */

var wtmg = '\
	<div id="wtmg-wrapper"> \
		<div id="wtmg-inner" class="clearfix"> \
			<table><tr> \
			<td id="wtmg-left"> \
				<h2>Where your money goes</h2> \
				<p>Legacy is an organisation that provides services to Australian families suffering after the injury or death of a spouse or parent, during or after their defence force service. We currently care for around 80,000 widows and 1,800 children and disabled dependants throughout Australia.</p> \
				<a href="http://www.legacy.com.au/OurWork">Find out more ></a> \
			</td> \
			<td id="wtmg-right"> \
			</td> \
		</tr></table> \
		</div> \
	</div> \
';


/*
 *
 * Add custom sharing
 *
 */

var share = '\
	<div class="share-container"> \
		<h3>Share this page:</h3> \
		<a class="facebook" href="#">Share on Facebook</a> \
		<a class="twitter" href="#">Share on Twitter</a> \
		<a class="linkedin" href="#">Share on Linkedin</a> \
		<div class="clearfix"></div> \
	</div> \
';

/*
 *
 * Personal public page
 *
 */
$.e2.addHandler(/^FundraisingPage$/, function() {

    // move page name to main content area
    var pagename = $.trim($('#page-name').html());
    $('#section-main-content').prepend('<h2 class="italic">' + pagename + '</h2>');

    // get image ID
    var imgageSrc;
    if ($('.media-manager-list li').length > 0) { // image has been uploaded
        if ($('.media-manager-list li').length === 1) { // only one image
            imgageSrc = '/registrant/' + $('.media-manager-list li img').attr('src');
        } else { // more than one image, grab the latest uploaded
            imgageSrc = '/registrant/' + $('.media-manager-list li:eq(0) img').attr('src');
        }

    }

    // set up profile pic
    $('#additionalContentArea').html('<div id="profile-pic-wrapper"><div id="profile-pic" style="background:url(' + imgageSrc + ')"></div></div>');
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_panelMyTeamHeading').appendTo('#additionalContentArea');
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_panelMyteamDetails').appendTo('#additionalContentArea');

    // move donate button
    $('#amount-raised-container button')
        .appendTo('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_asideThermometer')
        .clone()
        .css({
            'margin': '20px 0 0 0'
        })
        .appendTo($('.message-container'));

    // remove media carousel, conflicts with thermometer
    $('#fundraising-page-header').remove();

    $(document).ready(function($) {

        // paginate supporters/messages
        $('.timeline-item').each(function() {
            if ($(this).children('.text-container').children('.supporter-message').text().length > 0) {
                $(this).addClass('message');
                $(this).children('.text-container').children('.supporter-message').prepend('"').append('"');
            }
            var date = $(this).children('.text-container').children('.activity-date').text().replace('t', '').trim().split(',')[0];
            $(this).find('.supporter-name').prepend('<span>On ' + date + ' </span>');
        });
        $('.supporters-timeline-container').easyPaginate({
            paginateElement: '.timeline-item',
            elementsPerPage: 5,
            effect: 'climb'
        });

    });

    // add wtmg content
    $('#main-container').after(wtmg);

    // add share links
    $('.aside-thermometer').append(share);

    // trigger addthis links
    $('.share-container .facebook').click(function(e) {
        e.preventDefault();
        $('.addthis_button_facebook').trigger('click');
    });
    $('.share-container .twitter').click(function(e) {
        e.preventDefault();
        $('.addthis_button_twitter').trigger('click');
    });
    $('.share-container .linkedin').click(function(e) {
        e.preventDefault();
        $('.addthis_button_linkedin').trigger('click');
    });

    // wrap first two words of ehader hearer text in span
    var words = $('#section-main-content h2.italic').html().split(' ');
    if (words.length > 2) {
        $('#section-main-content h2.italic').html(function(i, html) {
            return html.replace(/(\w+\s\w+)/, '<span>$1</span>')
        });
    } else {
        $('#section-main-content h2.italic').addClass('pink');
    }

});


/*
 *
 * Team public page
 *
 */
$.e2.addHandler(/^TeamFundraisingPage$/, function() {

    var pagename = $.trim($('#page-name').html());
    $('#section-main-content').prepend('<h2 class="italic">' + pagename + '</h2>');

    // get image ID
    var imgageSrc;
    if ($('.media-manager-list li').length > 0) { // image has been uploaded
        if ($('.media-manager-list li').length === 1) { // only one image
            imgageSrc = '/registrant/' + $('.media-manager-list li img').attr('src');
        } else { // more than one image, grab the latest uploaded
            imgageSrc = '/registrant/' + $('.media-manager-list li:eq(0) img').attr('src');
        }
    }

    // clippath for profile image
    var clippath = '\
		<svg width="0" height="0"> \
		  <clipPath id="clipPolygon"> \
		    <polygon points="0 230,230 215,230 0,0 0"> \
		    </polygon> \
		  </clipPath> \
		</svg> \
	';
    $('#profile-pic').after(clippath);

    // add profile pic and move content to leftnav
    $('#additionalContentArea').html('<div id="profile-pic-wrapper"><div id="profile-pic" style="background:url(' + imgageSrc + ')"></div></div><h3>Our team</h3>');

    $('#team-members-list').appendTo('#additionalContentArea');

    // move buttons
    $('#amount-raised-container button').appendTo('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_asideThermometer');

    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_buttonDonate')
        .clone()
        .css({
            'margin': '20px 0 0 0'
        })
        .appendTo($('.message-container'));

    // remove media carousel, conflicts with thermometer
    $('#fundraising-page-header').remove();

    // paginate supporters/messages
    $(document).ready(function($) {
        $('.timeline-item').each(function() {
            if ($(this).children('.text-container').children('.supporter-message').text().length > 0) {
                $(this).addClass('message');
                $(this).children('.text-container').children('.supporter-message').prepend('"').append('"');
            }
            var date = $(this).children('.text-container').children('.activity-date').text().replace('t', '').trim().split(',')[0];
            $(this).find('.supporter-name').prepend('<span>On ' + date + ' </span>');
        });

        $('.captain-label').prependTo($('.captain-label').prev('.text-container'));

        $('.supporters-timeline-container').easyPaginate({
            paginateElement: '.timeline-item',
            elementsPerPage: 5,
            effect: 'climb'
        });

    });

    // add wtmg content
    $('#main-container').after(wtmg);

    // add share links
    $('.aside-thermometer').append(share);

    // trigger addthis links
    $('.share-container .facebook').click(function() {
        $('.addthis_button_facebook').trigger('click');
    });
    $('.share-container .twitter').click(function() {
        $('.addthis_button_twitter').trigger('click');
    });
    $('.share-container .linkedin').click(function(e) {
        e.preventDefault();
        $('.addthis_button_linkedin').trigger('click');
    });

    $(document).ready(function() {
        var teamid = $('#team-members-list-container').data('team-id');
        var teamname = $('#section-main-content h2.italic').text().trim();
        var location = $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_divLocationInfo .event-location-value').html().trim();

        $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_buttonJoinTeam')
            .after('<a class="join_team" href="' + MAINSITE_LINK + 'registration#/?teamID=' + teamid + '&teamname=' + teamname + '&location=' + location + '">Join this team ></a>');
    });


    // wrap first two words of ehader hearer text in span
    var words = $('#section-main-content h2.italic').html().split(' ');
    if (words.length > 2) {
        $('#section-main-content h2.italic').html(function(i, html) {
            return html.replace(/(\w+\s\w+)/, '<span>$1</span>')
        });
    } else {
        $('#section-main-content h2.italic').addClass('pink');
    }

});


/*
 *
 * Corporate Team public page
 *
 */
$.e2.addHandler(/^CorporateTeamFundraisingPage/, function() {

    var pagename = $.trim($('#page-name').html());
    $('#section-main-content').prepend('<h2 class="italic">' + pagename + '</h2>');

    // get image ID
    var imgageSrc;
    if ($('.media-manager-list li').length > 0) { // image has been uploaded
        if ($('.media-manager-list li').length === 1) { // only one image
            imgageSrc = '/registrant/' + $('.media-manager-list li img').attr('src');
        } else { // more than one image, grab the latest uploaded
            imgageSrc = '/registrant/' + $('.media-manager-list li:eq(0) img').attr('src');
        }
    }

    // add profile pic and move content to leftnav
    $('#additionalContentArea').html('<div id="profile-pic-wrapper"><div id="profile-pic" style="background:url(' + imgageSrc + ')"></div></div><h3>Our team</h3>');
    $(window).load(function() {

        var tc = $('.captain-labelA').html();
        console.log(tc);
    })
    // $('.captain-labelA').addClass('sadffsadf').prependTo($('.captain-labelA').prev('.text-container'));
    $('#team-members-list').appendTo('#additionalContentArea');

    // move buttons
    $('#amount-raised-container button').appendTo('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_asideThermometer');

    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_buttonDonate')
        .clone()
        .css({
            'margin': '20px 0 0 0'
        })
        .appendTo($('.message-container'));

    // remove media carousel, conflicts with thermometer
    $('#fundraising-page-header').remove();

    // paginate supporters/messages
    $(document).ready(function($) {
        $('.timeline-item').each(function() {
            if ($(this).children('.text-container').children('.supporter-message').text().length > 0) {
                $(this).addClass('message');
                $(this).children('.text-container').children('.supporter-message').prepend('"').append('"');
            }
            var date = $(this).children('.text-container').children('.activity-date').text().replace('t', '').trim().split(',')[0];
            $(this).find('.supporter-name').prepend('<span>On ' + date + ' </span>');
        });

        $('.supporters-timeline-container').easyPaginate({
            paginateElement: '.timeline-item',
            elementsPerPage: 5,
            effect: 'climb'
        });

    });

    // add wtmg content
    $('#main-container').after(wtmg);

    // add share links
    $('.aside-thermometer').append(share);

    // trigger addthis links
    $('.share-container .facebook').click(function() {
        $('.addthis_button_facebook').trigger('click');
    });
    $('.share-container .twitter').click(function() {
        $('.addthis_button_twitter').trigger('click');
    });
    $('.share-container .linkedin').click(function(e) {
        e.preventDefault();
        $('.addthis_button_linkedin').trigger('click');
    });

    var teamid = $('#team-members-list-container').data('corporate-team-id');
    var teamname = $('#section-main-content h2.italic').html().trim();
    // var location= = $('.event-location-value').html().trim();
    // $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_buttonJoinTeam')
    //   .after('<a class="join_team" href="' + MAINSITE_LINK + 'registration#/?CorporateTeamID=' + teamid + '&teamname=' + teamname + '">Join this team ></a>');

    // wrap first two words of ehader hearer text in span
    var words = $('#section-main-content h2.italic').html().split(' ');
    if (words.length > 2) {
        $('#section-main-content h2.italic').html(function(i, html) {
            return html.replace(/(\w+\s\w+)/, '<span>$1</span>')
        });
    } else {
        $('#section-main-content h2.italic').addClass('pink');
    }

});


/*
 *
 *  Search page
 *
 */
$.e2.addHandler(/^search/, function() {

    // add placeholders to search page inputs
    $('#individualSearchFirstName_ctl').val('').attr('placeholder', 'First name');
    $('#individualSearchLastName_ctl').val('').attr('placeholder', 'Last name');
    $('#individualSearchPageName_ctl').val('').attr('placeholder', 'Start typing the page name');

    $('#teamNameSearch_ctl').val('').attr('placeholder', 'Start typing the team name');
    $('#teamCaptainSearchFirstName_ctl').val('').attr('placeholder', 'First name');
    $('#teamCaptainSearchLastName_ctl').val('').attr('placeholder', 'Last name');
    $('#pageNameSearch_ctl').val('').attr('placeholder', 'Start typing the page name');

    // remove page name option from team filter
    $('#ctl00_ctl00_mainContent_bodyContentPlaceHolder_drpSearchTeamSelection option:eq(2)').remove();

});


/****************************************************************************************************************************/
/*  Page functions                                                                                                          */
/****************************************************************************************************************************/
function addCommas(amount) {

    amount += '';
    var splitAmount = amount.split('.');
    var dollars = splitAmount[0];
    var cents = splitAmount.length > 1 ? '.' + splitAmount[1].substr(0, 2) : '';
    var regex = /(\d+)(\d{3})/;
    while (regex.test(dollars)) {
        dollars = dollars.replace(regex, '$1' + ',' + '$2');
    }
    return '$' + dollars;

}
