module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // General concatentation
        // concat: {
        //     js: {
        //         options: {
        //             separator: ';'
        //         },
        //         src: ['js/*.js'],
        //         dest: 'public/js/main.js'
        //     }
        // },

        // CSS
        compass: {
            dist: {
                options: {
                    sassDir: 'sass',
                    cssDir: 'frontstream/css',
                    outputStyle: 'compressed'
                }
            }
        },

        // JS
        // browserify: {
        //     dist: {
        //         files: {
        //             'public/js/main.js': ['js/*.js']
        //         }
        //     }
        // },

        uglify: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: '.',
                    src: 'js/*.js',
                    dest: 'frontstream/'
                }]
            }
        },

        watch: {
            css: {
                files: 'sass/*.scss',
                tasks: 'compass:dist'
            },
            js: {
                files: ['js/*.js'],
                tasks: ['uglify']
            },
        }

    });

}
