## Development notes                    

  - Files live on the S3 bucket - s3://hmd/rmwp/
  - Before starting any development you need to download all files to a local envirenment
  - Run 'compass watch' in the root directory for SCSS generation
  - CSS and JS files live in the assets directory, when developing it's easier to just copy the assets directory to S3
  - Files need to be manually copied back to S3 when finished to keep S3 up to date


## Desktop DOM manipulations    

  - LoginRegister.aspx

    - Moved form components in left nav to main content area;
    - appended a button to trigger facebook login  


  - home.aspx

      - overwritten the media carousel so that only the default or last uploaded photo shows


  - CheckResults.aspx

    - removed the default pie chart and replaced with custom thermometer/guage


  - ManageSponsors.aspx

    - removed the default pie chart and replaced with custom thermometer/guage


  - selfSponsor.aspx

    - injected html content for price points
    - hidden artez amount field, and update it with custom amount input or price point click
    - added custom card type select icons, which is used to trigger the artez gateway radio options for paypal or traditional payments which are hidden, credit card types trigger the artez default select for cart type which is also hidden from view
    - there is also functionality to restore page state if there are validation errors when page is submitted


  - TeamResults.aspx

    - removed the default pie chart and replaced with custom thermometer/guage


  - ManageTeamSponsors.aspx

    - removed the default pie chart and replaced with custom thermometer/guage


  - FundraisingPage.aspx

    - override the default media carousel, and display a single image in left column
    - hidden the default thermometer chart and replaced with custom thermometer/guage
    - added a pagination plugin for messages


  - TeamFundraisingPage.aspx

    - override the default media carousel, and display a single image in left column
    - hidden the default thermometer chart and replaced with custom thermometer/guage
    - added a pagination plugin for messages
    - moved team members to left column


  - donate.aspx

    - moved suggested amounts to top of form and styled as price points
    - moved some form fields around to make form more intuitive
    - added custom card type select icons, which is used to trigger the artez gateway radio options for paypal or traditional payments which are hidden, credit card types trigger the artez default select for cart type which is also hidden from view
    - there is also functionality to restore page state if there are validation errors when page is submitted
    - made error container absolutely positioned to preserve page styling


  - Global functions

      - inject logo with hubsite link in header
      - add search link in header


## Aretz - Mobile DOM manipulations

  - mobileDonate.aspx

      - injected labels for form fields
      - injected html content for price points
      - added custom card type select icons, which is used to trigger the artez gateway radio options for paypal or traditional payments which are hidden, credit card types trigger the artez default select for cart type which is also hidden from view
      - there is also functionality to restore page state if there are validation errors when page is submitted


  - mobilePersonalPage.aspx

      - hidden the default thermometer chart and replaced with custom thermometer/guage
      - added Where the money goes content


  - mobileTeamPage.aspx

      - hidden the default thermometer chart and replaced with custom thermometer/guage
      - added Where the money goes content


  - global functions

      - injected html content for a site footer
